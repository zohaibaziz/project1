clear all;
close all;

load_sigma_rel=[20 40 60];
TVE_accuracy   = [0.001, 0.005, 0.01];
range_PMU=[1:16];
range_buses=[1:33];
t=384;
n_buses=33;
cd('case33')
for q = 1:length(load_sigma_rel),                   
         s1=sprintf('WLS_state_estimation_variable_PMU_greedy_NoPeaks_3_LoadMax_%2.1f%%.mat',load_sigma_rel(q));
         s2=sprintf('KF_state_estimation_variable_PMU_greedy_NoPeaks_3_LoadMax_%2.1f%%.mat',load_sigma_rel(q));
         
         WLS(q)=load(s1); 
         KF(q)=load(s2);
end;
cd ..,

%--------------------------------------------------------------------------
%plot_diagrams_99_percentile_20%,40%,60%Uncertainties_fix_TVEs_b/w_wls_and_KF
%--------------------------------------------------------------------------
for i=1:length(TVE_accuracy),
    for j=1:length(load_sigma_rel),
        for k=1:length(range_PMU),
            perc1_99(k) = quantile(reshape(WLS(j).Error.Voltage(:,:,k,i)',1,t*n_buses),0.99);
            perc2_99(k) = quantile(reshape(KF(j).Error.Voltage(:,:,k,i)',1,t*n_buses),0.99);
        end    
       
figure(i)
x=plot(perc1_99,'-o');
hold on
y=plot(perc2_99,'--x');
legend('WLS Max. load variation +/-20%','KF Max. load variation +/-20%','WLS Max. load variation +/-40%','KF Max. load variation +/-40%','WLS Max. load variation +/-60%','KF Max. load variation +/-60%');
colormap gray
xlabel('Number of PMUs')
ylabel(sprintf('99-percentile-Error-Voltage%2.1f%% TVE',TVE_accuracy(i)*100))
title(sprintf('Comparison between WLS and EKF with %2.1f%% TVE and 20,40,60 percent relative uncertainities',TVE_accuracy(i)*100))
    end 
end

%--------------------------------------------------------------------------
%semilogy_diagrams_99_percentile_20%,40%,60%Uncertainties_fix_TVEs_b/w_wls_and_KF
%--------------------------------------------------------------------------
for i=1:length(TVE_accuracy),
    for j=1:length(load_sigma_rel),
        for k=1:length(range_PMU),
            perc1_99(k) = quantile(reshape(WLS(j).Error.Voltage(:,:,k,i)',1,t*n_buses),0.99);
            perc2_99(k) = quantile(reshape(KF(j).Error.Voltage(:,:,k,i)',1,t*n_buses),0.99);
        end    
       
figure(i+3)
x=semilogy(perc1_99,'-o');
hold on
y=semilogy(perc2_99,'--x');
legend('WLS Max. load variation +/-20%','KF Max. load variation +/-20%','WLS Max. load variation +/-40%','KF Max. load variation +/-40%','WLS Max. load variation +/-60%','KF Max. load variation +/-60%');
colormap gray
xlabel('Number of PMUs')
ylabel(sprintf('99-percentile-Error-Voltage%2.1f%% TVE',TVE_accuracy(i)*100))
title(sprintf('Comparison between WLS and EKF with %2.1f%% TVE and 20,40,60 percent relative uncertainities',TVE_accuracy(i)*100))
    end 
end

