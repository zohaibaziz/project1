clear all;
close all;

grid_type = 'case33';

cd(grid_type);
placement_0 = load('sequence_PMU_0');
placement = load('sequence_PMU');

cd ..

No_peaks = 1;                                          % Number of peaks of the distribution assuming GMM
load_sigma_rel = [0.2 0.4 0.6]/3;

TVE_accuracy   = [0.001, 0.005, 0.01];                          % worst-case TVE boundary
PMU_amp_std    = TVE_accuracy./(sqrt(2*log(1/(1-0.99))));       % relative amp. std. dev. TVE_accuracy = 99% percentile
PMU_ang_std    = PMU_amp_std;                                   % angular amp. std. dev.  
           
No_PMU_accuracy_values = length(TVE_accuracy);
No_load_conditions = length(load_sigma_rel);
No_load_peaks = length(No_peaks);

No_PMU = 1:numel(placement.norm{1,1})+1;
No_PMU_short = 1:numel(placement.norm{1,1})-4;
options_init = optimoptions('lsqcurvefit');
options = optimoptions(options_init,'FunctionTolerance',1e-8);

for q = 1:No_load_conditions 
    for d = 1:No_PMU_accuracy_values        
        Uncertainty(:,q,d) = [placement_0.norm{q,d} placement.norm{q,d}];
        %X(:,q,d) = lsqcurvefit(@(x,No_PMU_short)x(1)./No_PMU_short.^x(2)+x(3),[placement.norm{q,d}(1) 1 0],No_PMU_short,Uncertainty(1:numel(No_PMU_short),q,d)',[], [], options)        
        MSE = @(x) (log(x(1)./No_PMU.^x(2)+x(3)) - log(Uncertainty(:,q,d)'))*(log(x(1)./No_PMU.^x(2)+x(3)) - log(Uncertainty(:,q,d)'))'; 
        X(:,q,d) = fminsearch(@(x) MSE(x),[placement.norm{q,d}(1) 1 0]);
        Uncertainty_approx(:,q,d) = X(1,q,d)./No_PMU.^(X(2,q,d))+X(3,q,d);
    end;    
end;

figure(1);
index_1=2;
index_2=3;
semilogy(No_PMU,Uncertainty(:,1,index_1),'k:x',No_PMU,Uncertainty(:,2,index_1),'k:o',...
         No_PMU,Uncertainty(:,3,index_1),'k:d', No_PMU,Uncertainty_approx(:,1,index_1),'k-',...
         No_PMU,Uncertainty_approx(:,2,index_1),'k-',No_PMU,Uncertainty_approx(:,3,index_1),'k-');
xlabel('No. of PMUs'); ylabel('u_M(C)');
legend('Max. load variation \pm20%','Max. load variation \pm40%','Max. load variation \pm60%')
axis([1 32 1e-4 1e-2])
 

%figure(2);
%plot(No_PMU,Uncertainty(:,1,1),'k-',No_PMU,Uncertainty(:,2,2),'k--',No_PMU,Uncertainty(:,3,3),'k:')

