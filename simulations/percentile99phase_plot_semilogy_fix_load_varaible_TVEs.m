clear all;
close all;

load_sigma_rel=[20 40 60];
TVE_accuracy   = [0.001, 0.005, 0.01];
range_PMU=[1:16];
range_buses=[1:32];
t=384;
n_buses=32;
%cd('case33');
cd('casedummy');
for q = 1:length(load_sigma_rel),                   
         s1=sprintf('WLS_state_estimation_variable_PMU_greedy_NoPeaks_3_LoadMax_%2.1f%%.mat',load_sigma_rel(q));
         s2=sprintf('KF_state_estimation_variable_PMU_greedy_NoPeaks_3_LoadMax_%2.1f%%.mat',load_sigma_rel(q));
         
         WLS(q)=load(s1); 
         KF(q)=load(s2);
end;
cd ..

%--------------------------------------------------------------------------
%plot_diagrams_99_percentile_0.1%,0.5%,1%TVE_fix_Max.Load_Uncertainties_b/w_wls_and_KF
%--------------------------------------------------------------------------
for i=1:length(load_sigma_rel),
    for j=1:length(TVE_accuracy),
        for k=1:length(range_PMU),
            perc1_99(k) = quantile(reshape(WLS(i).Error.Phase(:,:,k,j)',1,t*n_buses),0.99);
            perc2_99(k) = quantile(reshape(KF(i).Error.Phase(:,:,k,j)',1,t*n_buses),0.99);
        end    
    
figure(i)
x=plot(perc1_99*180/pi,'-o');
hold on
y=plot(perc2_99*180/pi,'--x');
legend('WLS 0.1%TVE','KF 0.1%TVE','WLS 0.5%TVE','KF 0.5%TVE','WLS 1%TVE','KF 1%TVE');
colormap gray
xlabel('Number of PMUs')
ylabel(sprintf('99-percentile-Error-Phase-%2.1f%%',load_sigma_rel(i)))
title(sprintf('Plots between WLS and EKF with 0.1,0.5 and 1 percent TVE  Max. Load %2.1f%%',load_sigma_rel(i)))

    end
end

%--------------------------------------------------------------------------
%Semilogy_diagrams_99_percentile_0.1%,0.5%,1%TVE_fix_Max.Load_Uncertainties_b/w_wls_and_KF
%--------------------------------------------------------------------------
for i=1:length(load_sigma_rel),
    for j=1:length(TVE_accuracy),
        for k=1:length(range_PMU),
            perc1_99(k) = quantile(reshape(WLS(i).Error.Phase(:,:,k,j)',1,t*n_buses),0.99);
            perc2_99(k) = quantile(reshape(KF(i).Error.Phase(:,:,k,j)',1,t*n_buses),0.99);
        end    
    
figure(i+3)
x=semilogy(perc1_99*180/pi,'-o');
hold on
y=semilogy(perc2_99*180/pi,'--x');
legend('WLS 0.1%TVE','KF 0.1%TVE','WLS 0.5%TVE','KF 0.5%TVE','WLS 1%TVE','KF 1%TVE');
colormap gray
xlabel('Number of PMUs')
ylabel(sprintf('99-percentile-Error-Phase-%2.1f%%',load_sigma_rel(i)))
title(sprintf('Semilogy between WLS and EKF with 0.1,0.5 and 1 percent TVE Max. Load %2.1f%%',load_sigma_rel(i)))

    end
end


