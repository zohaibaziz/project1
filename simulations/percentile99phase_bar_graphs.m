clear all;
close all;

load_sigma_rel=[20 40 60];
range_PMU=[1:16];
range_buses=[1:32];
t=384;
n_buses=32;
cd('case33')
for q = 1:length(load_sigma_rel),                   
         s1=sprintf('WLS_state_estimation_variable_PMU_greedy_NoPeaks_3_LoadMax_%2.1f%%.mat',load_sigma_rel(q));
         s2=sprintf('KF_state_estimation_variable_PMU_greedy_NoPeaks_3_LoadMax_%2.1f%%.mat',load_sigma_rel(q));
         
         WLS(q)=load(s1); 
         KF(q)=load(s2);
end;
cd ..,

%--------------------------------------------------------------------------
%bar_diagram_99_percentile_20%,40%,60%Uncertainties_0.1%TVE_b/w_wls_and_KF
%--------------------------------------------------------------------------
for i=1:length(load_sigma_rel),
    for k=1:length(range_PMU)
        perc1_99(k) = quantile(reshape(WLS(i).Error.Phase(:,:,k,1)',1,t*n_buses),0.99);
        perc2_99(k) = quantile(reshape(KF(i).Error.Phase(:,:,k,1)',1,t*n_buses),0.99);
    end    
        perc1_99_values=perc1_99'*180/pi; 
        perc2_99_values=perc2_99'*180/pi; 
        
figure(i)
bar([perc1_99_values,perc2_99_values],'grouped','LineWidth',1.7);
legend('WLS','KF')
colormap gray
xlabel('Number of PMUs')
ylabel('99-percentile-Error-Phase-0.1%TVE') 
title(sprintf('Comparison between WLS and EKF with 0.1 percent TVE and LoadMax %2.1f%% w.r.t PMUs',load_sigma_rel(i)))
end

%--------------------------------------------------------------------------
%bar_diagram_99_percentile_20%,40%,60%Uncertainties_0.5%TVE_b/w_wls_and_KF
%--------------------------------------------------------------------------
for i=1:length(load_sigma_rel),
    for k=1:length(range_PMU)
        perc1_99(k) = quantile(reshape(WLS(i).Error.Phase(:,:,k,2)',1,t*n_buses),0.99);
        perc2_99(k) = quantile(reshape(KF(i).Error.Phase(:,:,k,2)',1,t*n_buses),0.99);
    end    
        perc1_99_values=perc1_99'*180/pi; 
        perc2_99_values=perc2_99'*180/pi; 
        
figure(i+3)
bar([perc1_99_values,perc2_99_values],'grouped','LineWidth',1.7);
legend('WLS','KF')
colormap gray
xlabel('Number of PMUs')
ylabel('99-percentile-Error-Phase-0.5%TVE')
title(sprintf('Comparison between WLS and EKF with 0.5 percent TVE and LoadMax %2.1f%% w.r.t PMUs',load_sigma_rel(i)))
end

%--------------------------------------------------------------------------
%bar_diagram_99_percentile_20%,40%,60%Uncertainties_1%TVE_b/w_wls_and_KF
%--------------------------------------------------------------------------
for i=1:length(load_sigma_rel),
    for k=1:length(range_PMU)
        perc1_99(k) = quantile(reshape(WLS(i).Error.Phase(:,:,k,3)',1,t*n_buses),0.99);
        perc2_99(k) = quantile(reshape(KF(i).Error.Phase(:,:,k,3)',1,t*n_buses),0.99);
    end    
        perc1_99_values=perc1_99'*180/pi; 
        perc2_99_values=perc2_99'*180/pi; 
        
figure(i+6)
bar([perc1_99_values,perc2_99_values],'grouped','LineWidth',1.7);
legend('WLS','KF')
colormap gray
xlabel('Number of PMUs')
ylabel('99-percentile-Error-Phase-1%TVE')
title(sprintf('Comparison between WLS and EKF with with 1 percent TVE and LoadMax %2.1f%% w.r.t PMUs',load_sigma_rel(i)))
end
